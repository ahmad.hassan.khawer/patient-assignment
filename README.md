# Patient Assignment
Minimal Spring Boot sample app.
Requirements

For building and running the application you need:

    JDK 1.11

Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the main method in the Application class from your IDE.

Alternatively you can use the Spring Boot Maven plugin like so:

`mvn spring-boot:run`

Currently database is using H2 Database. You can configure application with your desire database in `application.yaml` file in `main/resources`
