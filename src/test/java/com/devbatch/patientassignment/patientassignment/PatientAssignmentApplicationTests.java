package com.devbatch.patientassignment.patientassignment;

import com.devbatch.patientassignment.patientassignment.model.OutPatientDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(classes = PatientAssignmentApplication.class)
@AutoConfigureMockMvc
class PatientAssignmentApplicationTests {

  @Autowired
  public MockMvc mockMvc;

  @Test
  public void testGetOutpatientDetailsSuccessfully() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders
            .post("/api/v1/outPatient/")
            .content(asJsonString(new OutPatientDto()))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].patientId").exists());
  }

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
