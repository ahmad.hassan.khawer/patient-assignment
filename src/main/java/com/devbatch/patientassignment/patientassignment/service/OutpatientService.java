package com.devbatch.patientassignment.patientassignment.service;

import com.devbatch.patientassignment.patientassignment.model.OutPatientDto;

import java.util.List;


public interface OutpatientService {
  List<OutPatientDto> getOutpatientDetails(OutPatientDto outPatientDto);
}
