package com.devbatch.patientassignment.patientassignment.service;

import com.devbatch.patientassignment.patientassignment.domain.OutPatient;
import com.devbatch.patientassignment.patientassignment.mapper.OutPatientMapper;
import com.devbatch.patientassignment.patientassignment.model.OutPatientDto;
import com.devbatch.patientassignment.patientassignment.repository.OutpatientRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Business logic class for outpatient. Its middleware between controller and repository.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class OutpatientServiceImpl implements OutpatientService {

  private final OutpatientRepository outpatientRepository;
  private final OutPatientMapper outPatientMapper;

  /**
   * Finding all the outpatient base on criteria from data layer.
   *
   * @param outPatientDto criteria  param
   * @return list of outpatient records
   */
  @Override
  public List<OutPatientDto> getOutpatientDetails(OutPatientDto outPatientDto) {
    List<OutPatientDto> outPatientDtos = new ArrayList<>();
    List<OutPatient> data = outpatientRepository.findAllByTeamCodeAndHospAndDocCode(outPatientDto.getTeamCode(), outPatientDto.getHosp(), outPatientDto.getDocCode());

    for (OutPatient outPatient : data) {
      outPatientDtos.add(outPatientMapper.outPatientToOutPatientDto(outPatient));
    }

    // logging some information
    log.info("{} records found.", outPatientDtos.size());

    return outPatientDtos;
  }
}
