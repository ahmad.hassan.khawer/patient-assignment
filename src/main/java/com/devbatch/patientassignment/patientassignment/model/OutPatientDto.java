package com.devbatch.patientassignment.patientassignment.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Null;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;

/**
 * Data Transfer Object class to Outpatient class
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OutPatientDto {
  @Null(message = "Patient ID should be null")
  @Size(max = 10, message = "Patient ID should not exceed {max} characters")
  private String patientId;

  @Size(max = 10, message = "Clinic code should not exceed {max} characters")
  private String clinicCode;

  private OffsetDateTime appointmentDate;

  @Size(max = 30, message = "Reference reason should not exceed {max} characters")
  private String referenceReason;

  @Positive
  private String opBookingsId;

  @Size(max = 10, message = "Doc Code should not exceed {max} characters")
  private String docCode;

  private OffsetDateTime appointmentTime;

  private OffsetDateTime arriveTimeReception;

  private OffsetDateTime seenTimeStart;

  @Size(max = 10, message = "Appointment type should not exceed {max} characters")
  private String appointmentType;

  @Size(max = 4000, message = "Patient name should not exceed {max} characters")
  private String patientName;

  @Size(max = 30, message = "Service should not exceed {max} characters")
  private String service;

  @Size(max = 30, message = "Service should not exceed {max} characters")
  private String department;

  private OffsetDateTime seenTimeEnd;

  @Size(max = 500, message = "Booking comment should not exceed {max} characters")
  private String bookingComment;

  @Size(max = 20, message = "Book type should not exceed {max} characters")
  private String bookType;

  @Size(max = 20, message = "Book type1 should not exceed {max} characters")
  private String bookType1;

  @Size(max = 4000, message = "Doctor name should not exceed {max} characters")
  private String doctorName;

  private Long sessionId;

  private Long hosp;

  @Size(max = 10, message = "Team code should not exceed {max} characters")
  private String teamCode;
}
