package com.devbatch.patientassignment.patientassignment.mapper;

import com.devbatch.patientassignment.patientassignment.domain.OutPatient;
import com.devbatch.patientassignment.patientassignment.model.OutPatientDto;
import org.mapstruct.Mapper;

/**
 * Mapper to convert entity class to DTO and vice versa.
 */
@Mapper(uses = {DateMapper.class})
public interface OutPatientMapper {
  OutPatient outPatientDtoToOutPatient(OutPatientDto outPatientDto);

  OutPatientDto outPatientToOutPatientDto(OutPatient outPatient);
}
