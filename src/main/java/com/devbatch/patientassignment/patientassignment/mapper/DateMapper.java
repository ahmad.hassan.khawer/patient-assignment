package com.devbatch.patientassignment.patientassignment.mapper;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Objects;

/**
 * DateMapper used to convert date to offsetDateTime and vice versa.
 */
@Component
public class DateMapper {
  public OffsetDateTime asOffsetDateTime(Date date) {
    if (Objects.nonNull(date)) {
      return date.toInstant()
              .atZone(ZoneId.systemDefault())
              .toOffsetDateTime();
    }
    return null;
  }

  public Date asDate(OffsetDateTime offsetDateTime) {
    if (Objects.nonNull(offsetDateTime)) {
      return new Date(
              offsetDateTime.toInstant()
                      .toEpochMilli()
      );
    }
    return null;
  }
}
