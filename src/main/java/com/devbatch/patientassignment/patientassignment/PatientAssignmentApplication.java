package com.devbatch.patientassignment.patientassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatientAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatientAssignmentApplication.class, args);
	}
}
