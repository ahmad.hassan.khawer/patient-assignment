package com.devbatch.patientassignment.patientassignment.common;

import lombok.Getter;
import lombok.Setter;

/**
 * Common class to return data to client.
 */
@Getter
@Setter
public class ApiResponse<T> {
  private String message;
  private T data;

  public ApiResponse(String message, T data) {
    this.message = message;
    this.data = data;
  }

}
