package com.devbatch.patientassignment.patientassignment.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * Entity class for database table.
 */
@Entity(name = "TBL_OUTPATIENT")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class OutPatient {
  @Column(name = "PATID")
  @Id
  private String patientId;

  @Column(name = "CLINIC_CODE")
  private String clinicCode;

  @Column(name = "APPT_DATE")
  private Date appointmentDate;

  @Column(name = "REF_REASON")
  private String referenceReason;

  @Column(name = "OP_BOOKINGS_ID")
  private String opBookingsId;

  @Column(name = "DOC_CODE")
  private String docCode;

  @Column(name = "APPT_TIME")
  private Date appointmentTime;

  @Column(name = "ARRIVE_TIME_RECEPTION")
  private Date arriveTimeReception;

  @Column(name = "SEEN_TIME")
  private Date seenTimeStart;

  @Column(name = "APPT_TYPE")
  private String appointmentType;

  @Column(name = "PATIENT_NAME")
  private String patientName;

  @Column(name = "SERVICE")
  private String service;

  @Column(name = "DEPARTMENT")
  private String department;

  @Column(name = "SEEN_TIME_END")
  private Date seenTimeEnd;

  @Column(name = "BOOKING_COMMENT")
  private String bookingComment;

  @Column(name = "BOOK_TYPE")
  private String bookType;

  @Column(name = "BOOK_TYPE_1")
  private String bookType1;

  @Column(name = "DOC_NAME")
  private String doctorName;

  @Column(name = "SESSION_ID")
  private Long sessionId;

  @Column(name = "HOSP")
  private Long hosp;

  @Column(name = "TEAM_CODE")
  private String teamCode;
}
