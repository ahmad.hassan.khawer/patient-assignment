package com.devbatch.patientassignment.patientassignment.repository;

import com.devbatch.patientassignment.patientassignment.domain.OutPatient;
import com.devbatch.patientassignment.patientassignment.model.OutPatientDto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OutpatientRepository extends JpaRepository<OutPatient, String> {

  List<OutPatient> findAllByTeamCodeAndHospAndDocCode(String teamCode, Long hosp, String docCode);
}
