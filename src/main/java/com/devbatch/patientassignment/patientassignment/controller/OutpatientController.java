package com.devbatch.patientassignment.patientassignment.controller;

import com.devbatch.patientassignment.patientassignment.common.ApiResponse;
import com.devbatch.patientassignment.patientassignment.model.OutPatientDto;
import com.devbatch.patientassignment.patientassignment.service.OutpatientServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/outPatient")
@RequiredArgsConstructor
public class OutpatientController {

  private final OutpatientServiceImpl outpatientService;

  /**
   * This end point obtains current outpatient details from database
   * and return JSON response to client.
   *
   * @param outPatientDto request pay load
   * @return JSON response of obtained details.
   */
  @PostMapping(value = "/", produces = "application/json", consumes = "application/json")
  public ResponseEntity<ApiResponse> getOutpatientDetails(@RequestBody @Valid OutPatientDto outPatientDto) {
    return ResponseEntity.ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(new ApiResponse("Data retrieved successfully.", outpatientService.getOutpatientDetails(outPatientDto)));
  }
}
